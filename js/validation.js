function validate() {
  // error checker
  var isValidated = false;
  // get elements
  var name = document.getElementById("name");
  var email = document.getElementById("email");
  var subject = document.getElementById("subject");
  var message = document.getElementById("message");
  // validate elements
  if (name.value == "") {
    name.classList.add("error");
    return false;
  } else {
    name.classList.remove("error");
  }
  if (email.value == "") {
    email.classList.add("error");
    return false;
  } else {
    email.classList.remove("error");
  }
  if (subject.value == "") {
    subject.classList.add("error");
    return false;
  } else {
    subject.classList.remove("error");
  }
  if (message.value == "") {
    message.classList.add("error");
    return false;
  } else {
    message.classList.remove("error");
  }
}

function onKeyDownValidate(ele) {
  ele.value == "" ? ele.classList.add("error") : ele.classList.remove("error");
}
